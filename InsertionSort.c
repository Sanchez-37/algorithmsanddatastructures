#include <stdio.h>
void InsertionSort(int A[], int n){
	int key,j;
	//sort from left to right -> [sorted keys|unsorted keys]
	for(int i = 1; i < n; i++)
	{	//add the next key to the sorted part of the array
		key = A[i];
		j = i - 1;
		//to find the correct position in the sorted array
		while(j >= 0 && A[j] < key)
		{
			A[j+1] = A[j];
			j--;
		}
		//place the key in the correct position
		A[j + 1] = key;	 	
	}
}

void Print(int A[], int n){
	for(int i = 0; i < n; i++)
	{
		printf("%d,",A[i]);
	}
}

int main(int argc, char *argv[])
{
	int A[] = {5,2,4,6,1,3};
	printf("The Array before sorting: \n");
	Print(A, 6);

	printf("\nThe array after sorting: \n");
	InsertionSort(A, 6);
	Print(A,6);
}
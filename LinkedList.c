#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int size = 0;
struct Node {
	int val;
	struct Node *next;
};

int isEmpty(struct Node *n) {
	return n == NULL;
}

int ListSize() {
	return size;
}
/*n is a pointer to a pointer variable. Specifically a pointer to a pointer of a struct Node. *n allows us to access the memory address of the pointer
variable (head). And through this indirect addressing we are able to modify the links of the linked list.

*/
void addNode(struct Node **n,int val) {

	struct Node *newNode = (struct Node*) malloc(sizeof(struct Node));
	newNode->val = val;

	//note that *n gives us access to the pointer variable (head). This pointer variable has the address of the first element in the linked list. 
	//Therefore the new node will replace the first element of the list.
	newNode->next = *n;
	
	//we must then change the head reference (*n) so that it points at the newNode. *n will hold the address of newNode (since newNode is a pointer).
	*n = newNode;

	size++;
	
}

void removeNodeAtPositionGiven(struct Node **n, int pos) {
	//don't remove anything if list is empty or if the position is bigger than the number of nodes or position is negative.
	if (*n == NULL || pos >= size || pos < 0) {
		return;
	}
	//hold the address of the head node since we might want to free it up from memory later.
	struct Node *prev = NULL;
	struct Node *curr = *n;

	while (pos > 0) {
		prev = curr;
		curr = curr->next;
		pos--;
	}

	//remove head node
	if (*n == curr) {
		*n = curr->next;
	}
	//node is not in head
	else {
		prev->next = curr->next;
	}

	free(curr);
	size--;
	return;
	
	
}



void printList(struct Node *n) {
	while (n != NULL) {
		printf("%d ", n->val);
		n = n->next;
	}
}

int main(int argc, char *argv[]){

	//note that head variable is "holding" null. The behavior of *ptr is undefined since head did not store an object's address
	struct Node *head = NULL;
	
	if (isEmpty(head)) {
		printf("The List is empty\n");
	}

	addNode(&head, 3);
	addNode(&head, 1);
	addNode(&head, 4);

	printList(head);
	printf("\n");
	removeNodeAtPositionGiven(&head, 1);
	printf("After removing the second node: \n");
	printList(head);
	printf("\n");
	removeNodeAtPositionGiven(&head, 0);
	printList(head);
	printf("\n");
}
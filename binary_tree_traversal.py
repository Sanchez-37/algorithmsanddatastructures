class Tree:
    def __init__(self,x: list):
        self.nodes = []
        self.size = 0

    def add(self):
        pass

    def remove(self):
        pass

    def pop(self):
        pass

    def _generate_tree(self):
        for i,val in enumerate(x):
            left = 2*i + 1
            right = 2*i + 2
            node = TreeNode(val)
            if left < len(x):
                node.left = x[left]
            if right < len(x) - 1: 
                node.right = x[right]
            self.nodes.append(node)
            
    def print(self):
        for i in range(len(self.nodes)):
            print(self.nodes[i], self.nodes[i].left, self.nodes[i].right)
            
class TreeNode:
    def __init__(self,x):
        self.value = x
        self.left = None
        self.right = None

class solution:
    def __init__(self):
        self.list_of_nodes = []
    def preorder_traverse(self,root: TreeNode):
        if root is None:
            return
        self.list_of_nodes.append(root)
        self.preorder_traverse(root.left)
        self.preorder_traverse(root.right)
        return self.list_of_nodes

t = [1,6,2,3]
tree = Tree(t)
tree.print()
    
sol = solution()
print(sol.preorder_traverse(tree.nodes[0]))
